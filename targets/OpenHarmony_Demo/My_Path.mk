# Topdir
LITEOSTOPDIR := ../../
LITEOSTOPDIR := $(realpath $(LITEOSTOPDIR))

# Common
C_SOURCES   +=  $(wildcard $(LITEOSTOPDIR)/kernel/src/*.c) \
				$(wildcard $(LITEOSTOPDIR)/kernel/src/mm/*.c) \
				$(wildcard $(LITEOSTOPDIR)/components/cpup/*.c) \
				$(wildcard $(LITEOSTOPDIR)/components/power/*.c) \
				$(wildcard $(LITEOSTOPDIR)/components/backtrace/*.c) \
				$(wildcard $(LITEOSTOPDIR)/components/exchook/*.c) \
				$(wildcard $(LITEOSTOPDIR)/components/signal/*.c) \
                $(wildcard $(LITEOSTOPDIR)/utils/*.c)

C_INCLUDES  +=  -I$(LITEOSTOPDIR)/utils \
				-I$(LITEOSTOPDIR)/kernel/include \
				-I$(LITEOSTOPDIR)/components/cpup \
				-I$(LITEOSTOPDIR)/components/power \
				-I$(LITEOSTOPDIR)/components/backtrace \
				-I$(LITEOSTOPDIR)/components/exchook \
				-I$(LITEOSTOPDIR)/components/signal

# Third party related
C_SOURCES    += $(wildcard $(LITEOSTOPDIR)/third_party/bounds_checking_function/src/*.c)\
				$(wildcard $(LITEOSTOPDIR)/kal/cmsis/*.c)\
				$(wildcard $(LITEOSTOPDIR)/kal/posix/src/*.c)

C_INCLUDES   += -I$(LITEOSTOPDIR)/third_party/bounds_checking_function/include \
				-I$(LITEOSTOPDIR)/third_party/bounds_checking_function/src\
				-I$(LITEOSTOPDIR)/third_party/cmsis/CMSIS/RTOS2/Include \
				-I$(LITEOSTOPDIR)/third_party/musl/porting/liteos_m/kernel/include\
				-I$(LITEOSTOPDIR)/kal/cmsis \
				-I$(LITEOSTOPDIR)/kal/posix/include \
				-I$(LITEOSTOPDIR)/kal/posix/musl_src/internal

# My file
C_SOURCES    += 

C_INCLUDES   += 

# Arch related
ASM_SOURCES   += $(wildcard $(LITEOSTOPDIR)/arch/arm/cortex-m4/gcc/*.s)

ASMS_SOURCES  += $(wildcard $(LITEOSTOPDIR)/arch/arm/cortex-m4/gcc/*.S)

C_SOURCES     += $(wildcard $(LITEOSTOPDIR)/arch/arm/cortex-m4/gcc/*.c)

C_INCLUDES    += -I. \
                 -I$(LITEOSTOPDIR)/arch/include \
                 -I$(LITEOSTOPDIR)/arch/arm/cortex-m4/gcc

CFLAGS        += -nostdinc -nostdlib
ASFLAGS       += -imacros $(LITEOSTOPDIR)/kernel/include/los_config.h -DCLZ=CLZ

