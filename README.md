[TOC]



# OpenHarmony For MCU

## 一、仓库介绍
本仓库作为OpenHarmony移植到MCU的教程仓库，将持续更新分享包括STM32在内的MCU移植鸿蒙系统

## 二、文章列表

- VSCode 准备 STM32 开发环境：[CSDN文章链接](https://jeckxu666.blog.csdn.net/article/details/123600472)

- 移植 3.1 版本 LiteOS 到 STM32F407（正点原子探索者开发板）：[CSDN文章链接](https://jeckxu666.blog.csdn.net/article/details/123971394)

- 简单了解一下移植后工程的 LiteOS-M 内核启动流程：[CSDN文章链接](https://jeckxu666.blog.csdn.net/article/details/124156569)

- 持续更新中！帮助大家学习OpenHarmony，支持国产进步，喜欢请给个 Star ！

